// Search-btn 

const searchInputWrapper = document.getElementById("search-input-wrapper");
const searchIcon = document.getElementById("search_icon");

searchInputWrapper.style.display = "none"

    searchIcon.onclick = function () {
      if (searchInputWrapper.style.display !== "none" 
        && searchIcon.src !=="images_content/search_icon.png") {
            searchInputWrapper.style.display = "none";
            searchIcon.src ="images_content/search_icon.png";
      } else {
        searchInputWrapper.style.display = "flex";
        searchIcon.src ="images_content/mobile_menu_close.png";
      }
    };



// Img Slider 

const slide = document.querySelectorAll('.slide');
const rightBtn = document.querySelector('.right');
const leftBtn = document.querySelector('.left');
let current = 0;

function cls(){
    for(let i = 0; i < slide.length; i++ ){
        slide[i].style.display = 'none';
    }
}

rightBtn.addEventListener('click', function next(){
    cls();
    if(current === slide.length-1) current = -1;
    current++;

    slide[current].style.display = 'block';
});

leftBtn.addEventListener('click', function prev(){
    cls();
    if(current === 0) current = slide.length;
    current--;

    slide[current].style.display = 'block';
})

function start(){
    cls();
    slide[current].style.display = 'block';
}

start();


// Mobile Navbar menu show

const hamburger = document.querySelector(".hamburger");
const mainMenu = document.querySelector("#main_menu");
const content = document.querySelector("#content");

hamburger.addEventListener("click", () => {
    hamburger.classList.toggle("active");
    mainMenu.classList.toggle("active");
    content.classList.toggle("active");
});


//Mobile Navbar Click
const innerMenu = document.querySelectorAll(".inner_menu");
const dropMenu = document.querySelectorAll(".drop_menu");
const cross = document.querySelectorAll(".cross2");
const plus = document.querySelectorAll(".plus")

//Dropdown for div
    for (let i = 0; i < dropMenu.length; i++){
        dropMenu[i].addEventListener("click", function(){
            innerMenu[i].classList.toggle("active");
        });
    };

    for (let i = 0; i < dropMenu.length; i++){
        dropMenu[i].addEventListener("click", function(){
            cross[i].classList.toggle("active");
        });
    };


//Dropdown for plus mark
    for (let i = 0; i < plus.length; i++){
        plus[i].addEventListener("click", function(){
            innerMenu[i].classList.toggle("active");
        });
    };

    for (let i = 0; i < plus.length; i++){
        plus[i].addEventListener("click", function(){
            cross[i].classList.toggle("active");
        });
    };