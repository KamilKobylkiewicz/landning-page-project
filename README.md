![a screenshot presenting the front page of the project website](./assets/LandingPagePc.PNG)

# RWD Landing Page Project 

A project created in order to put into practice the skills gained during the Droptica Academy Internship Program.

## Technologies used:
    - HTML
    - CSS
    - JavaScrip

## Author: Kamil Kobyłkiewicz 